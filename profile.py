import hashlib
import random
from lxml import etree as ET

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# Import the InstaGENI library.
import geni.rspec.igext as ig
# Import the Emulab specific extensions.
import geni.rspec.emulab as emulab
# Import Emulab Ansible-specific extensions.
import geni.rspec.emulab.ansible
from geni.rspec.emulab.ansible import (Role, RoleBinding, Override, Playbook)

IMAGE    = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD"
FIXBS    = "sudo /local/repository/fixbs.sh"
HEAD_CMD = "sudo -u `geni-get user_urn | cut -f4 -d+` -Hi /bin/sh -c '/local/repository/emulab-ansible-bootstrap/head.sh >/local/logs/setup.log 2>&1'"

# Create a portal object,
pc = portal.Context()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

# Add some custom roles to the rspec; we will bind them to nodes later.
request.addRole(
    Role("scos-sensor",path="ansible",playbooks=[
        Playbook("scos-sensor",path="scos-sensor.yml",become="root")]))

def findName(list, key):
    for pair in list:
        if pair[0] == key:
            return pair[1]
        pass
    return None

other_endpoints = [
    ('skull2','skull2'),
    ('cnode-moran','cnode-moran'),
    ('cnode-ustar','cnode-ustar'),
    ('cnode-ebc','cnode-ebc'),
    ('cnode-guesthouse','cnode-guesthouse'),
    ('cnode-mario','cnode-mario'),
]

# A list of endpoint sites.
endpoints = [
    ('urn:publicid:IDN+bookstore.powderwireless.net+authority+cm',
     "Bookstore"),
    ('urn:publicid:IDN+cpg.powderwireless.net+authority+cm',
     "CPG"),
    ('urn:publicid:IDN+ebc.powderwireless.net+authority+cm',
     "EBC"),
    ('urn:publicid:IDN+guesthouse.powderwireless.net+authority+cm',
     "Guesthouse"),
    ('urn:publicid:IDN+humanities.powderwireless.net+authority+cm',
     "Humanities"),
    ('urn:publicid:IDN+law73.powderwireless.net+authority+cm',
     "Law"),
    ('urn:publicid:IDN+madsen.powderwireless.net+authority+cm',
     "Madsen"),
    ('urn:publicid:IDN+moran.powderwireless.net+authority+cm',
     "Moran"),
    ('urn:publicid:IDN+sagepoint.powderwireless.net+authority+cm',
     "Sagepoint"),
    ('urn:publicid:IDN+web.powderwireless.net+authority+cm',
     "WEB"),
    ('urn:publicid:IDN+test1.powderwireless.net+authority+cm',
     "Prototype"),
] + other_endpoints

# Request a specific FE
pc.defineParameter("FE", "Fixed Endpoint",
                   portal.ParameterType.STRING, endpoints[0][0], endpoints)
# And the nuc to use
pc.defineParameter("node_id", "Radio ID",
                   portal.ParameterType.STRING, "nuc1")

# Map password to ansible overrides.
request.addOverride(
    Override("scos_sensor_admin_password",source="password",source_name="perExptPassword"))

params = pc.bindParameters()

#
# One node
#
name = findName(endpoints, params.FE)
if name in [oe[0] for oe in other_endpoints]:
    node = request.RawPC(name)
    node.component_id = name
    node.component_manager_id = 'urn:publicid:IDN+emulab.net+authority+cm'
else:
    node = request.RawPC(name + "-" + params.node_id)
    node.component_id = params.node_id
    node.component_manager_id = params.FE

node.disk_image = IMAGE
node.bindRole(RoleBinding("scos-sensor"))
# FEs only, until Mike fixes blockstore problem.
node.addService(pg.Execute(shell="sh",command=FIXBS))
node.addService(pg.Execute(shell="sh",command=HEAD_CMD))
node.startVNC();

# Local blockstore for the database
bs = node.Blockstore(name + "-bs", "/opt")
# All available space
bs.size = "0GB"

# Admin password for the web interface
request.addResource(ig.Password("perExptPassword"))

tourDescription = \
  "This profile sets up scos-sensor on a host with an attached B210"

tourInstructions = \
 """
Once your experiment completes setup, you will be able to access the [scos-sensor web interface](https://{host-""" + name + """-""" + params.node_id + """}/) (user `admin`, password `{password-perExptPassword}`).
 """

tour = ig.Tour()
tour.Description(ig.Tour.TEXT,tourDescription)
tour.Instructions(ig.Tour.MARKDOWN,tourInstructions)
request.addTour(tour)

pc.printRequestRSpec(request)
