import hashlib
import random
from lxml import etree as ET

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# Import the InstaGENI library.
import geni.rspec.igext as ig
# Import the Emulab specific extensions.
import geni.rspec.emulab as emulab
# Import Emulab Ansible-specific extensions.
import geni.rspec.emulab.ansible
from geni.rspec.emulab.ansible import (Role, RoleBinding, Override, Playbook)

IMAGE    = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD"
FIXBS    = "sudo /local/repository/fixbs.sh"
HEAD_CMD = "sudo -u `geni-get user_urn | cut -f4 -d+` -Hi /bin/sh -c '/local/repository/emulab-ansible-bootstrap/head.sh >/local/logs/setup.log 2>&1'"

# Create a portal object,
pc = portal.Context()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

# Add some custom roles to the rspec; we will bind them to nodes later.
request.addRole(
    Role("scos-sensor",path="ansible",playbooks=[
        Playbook("scos-sensor",path="scos-sensor.yml",become="root")]))

# A list of radios on the Motherwhip.
radios = [
    ('cbrssdr1-bes', 'cbrssdr1-bes'),
    ('cbrssdr1-browning','cbrssdr1-browning'),
    ('cbrssdr1-dentistry','cbrssdr1-dentistry'),
    ('cbrssdr1-fm','cbrssdr1-fm'),
    ('cbrssdr1-honors','cbrssdr1-honors'),
    ('cbrssdr1-hospital','cbrssdr1-hospital'),
    ('cbrssdr1-meb','cbrssdr1-meb'),
    ('cbrssdr1-smt','cbrssdr1-smt'),
    ('cbrssdr1-ustar','cbrssdr1-ustar'),
    ('cellsdr1-bes','cellsdr1-bes'),
    ('cellsdr1-browning','cellsdr1-browning'),
    ('cellsdr1-dentistry','cellsdr1-dentistry'),
    ('cellsdr1-fm','cellsdr1-fm'),
    ('cellsdr1-honors','cellsdr1-honors'),
    ('cellsdr1-hospital','cellsdr1-hospital'),
    ('cellsdr1-meb','cellsdr1-meb'),
    ('cellsdr1-smt','cellsdr1-smt'),
    ('cellsdr1-ustar','cellsdr1-ustar'),
    ]

# Request a specific radio
pc.defineParameter("radioname", "BS Radio",
                   portal.ParameterType.STRING, radios[0][0], radios)

# Map password to ansible overrides.
request.addOverride(
    Override("scos_sensor_admin_password",source="password",source_name="perExptPassword"))

params = pc.bindParameters()

#
# The radio
#
radioname = params.radioname
radio = request.RawPC(radioname)
radio.component_id = radioname
radio.component_manager_id = 'urn:publicid:IDN+emulab.net+authority+cm'
radioiface = radio.addInterface();

#
# One node
#
node = request.RawPC("com-" + radioname)
node.hardware_type = "d430"
node.component_manager_id = 'urn:publicid:IDN+emulab.net+authority+cm'
node.disk_image = IMAGE
node.bindRole(RoleBinding("scos-sensor"))
node.addService(pg.Execute(shell="sh",command=HEAD_CMD))
node.startVNC();

# Local blockstore for the database
bs = node.Blockstore("com-bs", "/opt")
# All available space
bs.size = "0GB"

#
# Link between compute node and radio
#
nodeiface = node.addInterface();

if (radioname.startswith("cbrs") or radioname.startswith("cell") or
    radioname.startswith("ota") or radioname.startswith("oai")):
    radioiface.addAddress(pg.IPv4Address("192.168.40.2", "255.255.255.0"))
    nodeiface.addAddress(pg.IPv4Address("192.168.40.1", "255.255.255.0"))
else:
    radioiface.addAddress(pg.IPv4Address("192.168.10.2", "255.255.255.0"))
    nodeiface.addAddress(pg.IPv4Address("192.168.10.1", "255.255.255.0"))
    pass
link = request.Link(members = [radioiface, nodeiface])

# Admin password for the web interface
request.addResource(ig.Password("perExptPassword"))

tourDescription = \
  "This profile sets up scos-sensor on a base station X310 (compute node)"

tourInstructions = \
 """
Once your experiment completes setup, you will be able to access the [scos-sensor web interface](https://{host-""" + "com" + """-""" + radioname + """}/) (user `admin`, password `{password-perExptPassword}`).
 """

tour = ig.Tour()
tour.Description(ig.Tour.TEXT,tourDescription)
tour.Instructions(ig.Tour.MARKDOWN,tourInstructions)
request.addTour(tour)

pc.printRequestRSpec(request)
